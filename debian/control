Source: gst-plugins-bad1.0-contrib
Section: contrib/libs
Priority: optional
Maintainer: Maintainers of GStreamer packages <gst-plugins-bad1.0@packages.debian.org>
Uploaders: Sebastian Dröge <slomo@debian.org>,
           Sjoerd Simons <sjoerd@debian.org>
Build-Depends: debhelper,
               debhelper-compat (= 13),
               dpkg-dev (>= 1.15.1),
               pkg-config (>= 0.11.0),
               meson (>= 0.59),
               xvfb,
               xauth,
               libglib2.0-dev (>= 2.56),
               libgstreamer1.0-dev (>= 1.20.0),
               libgstreamer-plugins-base1.0-dev (>= 1.20.0),
               gstreamer1.0-plugins-base (>= 1.20.0),
               gstreamer1.0-plugins-good (>= 1.20.0),
               libfdk-aac-dev [amd64 arm64 armel armhf i386 mips64el mipsel ppc64el s390x]
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/gstreamer-team/gst-plugins-bad1.0-contrib.git
Vcs-Browser: https://salsa.debian.org/gstreamer-team/gst-plugins-bad1.0-contrib/
Homepage: https://gstreamer.freedesktop.org

Package: gstreamer1.0-fdkaac
Architecture: amd64 arm64 armel armhf i386 mips64el mipsel ppc64el s390x
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends}
XB-GStreamer-Version: ${gstreamer:Version}
XB-GStreamer-Elements: ${gstreamer:Elements}
XB-GStreamer-URI-Sources: ${gstreamer:URISources}
XB-GStreamer-URI-Sinks: ${gstreamer:URISinks}
XB-GStreamer-Encoders: ${gstreamer:Encoders}
XB-GStreamer-Decoders: ${gstreamer:Decoders}
Description: GStreamer FDK AAC plugins
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 GStreamer Bad Plug-ins is a set of plug-ins that aren't up to par compared
 to the rest. They might be close to being good quality, but they're missing
 something - be it a good code review, some documentation, a set of tests, a
 real live maintainer, or some actual wide use.
 .
 This package contains the FDK AAC plugins.
